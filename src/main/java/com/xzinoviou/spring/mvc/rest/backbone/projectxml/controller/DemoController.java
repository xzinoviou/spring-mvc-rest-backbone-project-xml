package com.xzinoviou.spring.mvc.rest.backbone.projectxml.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/** @author xzinoviou created 2/3/20 */
@RestController
@RequestMapping("/demo")
public class DemoController {

  @GetMapping
  public ResponseEntity<Map<String, String>> getDemo(@RequestParam String arg) {
    return ResponseEntity.ok(createDemoResponse(arg));
  }

  private Map<String, String> createDemoResponse(String arg) {
    Map<String, String> map = new HashMap<>();
    map.put("request", arg);
    return map;
  }
}
