A Spring MVC 5.x.x RELEASE REST API backbone project.

Config Method : XML.
Connection Pooling : Hikari
DB : MySQL 8

Anyone can use this as a backbone for instant local development projects.
In case you want ot change the DB or CP vendors , then changes should be made in the application.settings file and in the .xml configuration files , both located in the classpath.